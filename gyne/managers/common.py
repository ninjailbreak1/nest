from ..models import Course
from ..models import Question
from ..models import Team
from ..models import User
from ..models import Result
from ..models import QuestionType
from ..models import Form


def get_form_title(course_name):
    course_obj = Course.objects.get(name=course_name)
    return course_obj.active_form.title


def get_form_max_rate(form_title, course_name):
    course_obj = Course.objects.get(name=course_name)
    return Form.objects.get(course=course_obj, title=form_title).max_rate


def get_text_questions(form_title, course_name):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Form.objects.get(course=course_obj, title=form_title)

    return [qst.question_text for qst in Question.objects.filter(form=form_obj) if qst.is_multipoint == False]


def get_multipoints_questions(form_title_course_name):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Form.objects.get(course=course_obj, title=form_title)

    return [qst.question_text for qst in Questions.objects.filter(form=form_obj) if qst.is_multipoint == True]


def get_usernames(course_id: int):
    course_obj = Course.objects.get(id=course_id)
    course_teams = Team.objects.filter(course=course_obj)
    course_users = sum([list(User.objects.filter(team=team)) for team in course_teams], start=[])

    users_by_group = list()

    return [{'id': user.id, 'name': user.first_name} for user in course_users]
    for user in course_users:
        
        for tup in users_by_group:
            if user.team.name == tup[0]:
                tup[1].append("{first_name} {last_name}".format(
                    first_name=user.first_name, last_name=user.last_name))
                break
        else:
            users_by_group.append((user.team.name, ["{first_name} {last_name}".format(
                first_name=user.first_name, last_name=user.last_name)]))

    return users_by_group


def get_courses() -> list:
    return [{'id': x.id, 'name': x.name} for x in Course.objects.all()]


def get_question_desc(form_title, course_name):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Form.objects.get(course=course_obj, title=form_title)

    questions = Questions.objects.filter(form=form_obj)

    questions_description = list()

    for qst in questions:
        questions_description.append((qst.question_text, qst.descr))

    return qst


def set_data_for_user(form_name, question_text, course_name, author_first_name,
                      author_last_name, victim_first_name, victim_list_name, answer):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Forms.objects.get(from_title=form_name, course=course_obj)
    course_teams = Team.object.filter(course=course_obj)

    author_obj = User.objects.get(
        team=course_teams, first_name=author_first_name, last_name=author_last_name)
    victim_obj = User.objects.get(
        team=course_teams, first_name=victim_first_name, last_name=victim_last_name)
    question_obj = Question.objects.get(
        form=form_obj, question_text=question_text)

    r = Result(author=author_obj, victim=victim_obj,
               question=question, answer=answer)

    r.save()


def get_user(first_name, last_name, course_name):
    pass


"""
def get_groups(course_name):
    return Team.objects.filter(name=course_name) 
    
def get_group_hanichim(course_name, group_number):
    course_obj = Course.objects.filter(name=course_name)
    team_obj = Team.objects.filter(course=course_obj, number=group_number)
    return User.objects.filter(course=course_obj, team=team_obj)

def get_all_hanichim(course_name):
    course_obj = Course.objects.filter(name=course_name)

    return User.objects.filter(course=course_obj)
"""
