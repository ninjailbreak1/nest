from ..models import Course, Form, User, Question, Team, Result


def get_usernames(course_id: int, team_id: int = -1):
    course_obj = Course.objects.get(id=course_id)
    course_teams = Team.objects.filter(course=course_obj)
    course_users = sum([list(User.objects.filter(team=team))
                        for team in course_teams], start=[])

    if team_id != -1:
        return [{'id': user.id, 'name': f"{user.first_name} {user.last_name}"} for user in course_users if user.team.id == team_id]

    return [{'id': user.id, 'name': f"{user.first_name} {user.last_name}"} for user in course_users]


def get_question_desc(form_title, course_name):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Form.objects.get(course=course_obj, title=form_title)

    questions = Questions.objects.filter(form=form_obj)

    questions_description = list()

    for qst in questions:
        questions_description.append((qst.question_text, qst.descr))

    return qst


def set_data_for_user(form_name, question_text, course_name, author_first_name,
                      author_last_name, victim_first_name, victim_list_name, answer):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Forms.objects.get(from_title=form_name, course=course_obj)
    course_teams = Team.object.filter(course=course_obj)

    author_obj = User.objects.get(
        team=course_teams, first_name=author_first_name, last_name=author_last_name)
    victim_obj = User.objects.get(
        team=course_teams, first_name=victim_first_name, last_name=victim_last_name)
    question_obj = Question.objects.get(
        form=form_obj, question_text=question_text)

    r = Result(author=author_obj, victim=victim_obj,
               question=question, answer=answer)

    r.save()
