from ..models import Course
from . import teams_manager

def get_courses() -> list:
    return [{'id': x.id, 'name': x.name} for x in Course.objects.all()]

def get_course(course_id: int):
    return {"name": Course.objects.get(id=course_id).name, "teams": teams_manager.get_teams(course_id)}