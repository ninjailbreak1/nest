from typing import List
from statistics import stdev, mean
from ..models import Question, User, Result, Course
from . import usernames_manager, teams_manager

def parse_results(results: dict):
    for answer in results['answers']:
        current_question_answered = Question.objects.get(id=answer["question_id"])
        for feedback in answer['feedbacks']:
            res = Result(
                         author=User.objects.get(id=results["user_id"]),
                         victim=User.objects.get(id=feedback["victim_id"]),
                         question_answered=current_question_answered,
                         answer=feedback["answer"]
                        )
            res.save()

def get_statistics(user_id: int):
    user_obj = User.objects.get(id=user_id)
    form_obj = user_obj.team.course.active_form
    questions = Question.objects.filter(form=form_obj)

    textual_results = Result.objects.filter(victim=user_obj, question_answered__is_multipoint=False)
    numeric_results = Result.objects.filter(victim=user_obj, question_answered__is_multipoint=True)
    
    numeric_questions = {}
    textual_questions = {}
    for rslt in numeric_results:
        if rslt.question_answered.question_text not in numeric_questions:
            numeric_questions[rslt.question_answered.question_text] = [[],[]]
        if rslt.author.team == user_obj.team:
            numeric_questions[rslt.question_answered.question_text][0].append(int(rslt.answer))
        else:
            numeric_questions[rslt.question_answered.question_text][1].append(int(rslt.answer))
        
    numeric_feedback = [{
        'name': question,
        'tavg': mean(rslts[0]) if len(rslts[0]) != 0 else 0,
        'gavg': mean(rslts[0]+rslts[1]),
        'tdev': stdev(rslts[0]) if len(rslts[0]) >= 2 else 0,
        'gdev': stdev(rslts[0]+rslts[1]),
        'tpre': 0,
        'gpre': 0 
    } for question, rslts in numeric_questions.items()]

    for rslt in textual_results:
        if rslt.question_answered.question_text not in textual_results:
            textual_questions[rslt.question_answered.question_text] = []
        textual_questions[rslt.question_answered.question_text].append(rslt.answer)

    textual_feedback = [{
        'name': question,
        'answers': rslts
    } for question, rslts in textual_questions.items()]

    return  {
        "name": f"{user_obj.first_name} {user_obj.last_name}",
        "team_name": user_obj.team.name,
        "numeric_feedback": numeric_feedback,
        "textual_feedback": textual_feedback
        }


def calculate_precentage(course_id, all_users_statistics, course_teams):
    course_obj = Course.objects.get(id=course_id)
    form_obj = course_obj.active_form
    all_form_questions = Question.objects.filter(form=form_obj)
    for qst in all_form_questions:
        user_ratings = list()
        for user_stats in all_users_statistics:
            for answer in user_stats["numeric_feedback"]:
                if answer["name"] == qst.question_text:
                    user_ratings.append((user_stats, answer["tavg"], answer["gavg"]))
        for team in course_teams:
            team_users = [(tuser[0], tuser[1]) for tuser in user_ratings if tuser[0]["team_name"] == team["name"]]
            team_users.sort(key=lambda x: x[1])
            for i, user in enumerate(team_users):
                for answer in user[0]["numeric_feedback"]:
                    if answer["name"] == qst.question_text:
                        answer["tpre"] = f"{i+1 / len(team_users) * 100}%"
        user_ratings.sort(key=lambda x: x[2])
        for i, user in enumerate(user_ratings):
            for answer in user[0]["numeric_feedback"]:
                if answer["name"] == qst.question_text:
                    answer["gpre"] = f"{i+1 / len(user_ratings) * 100}%"
    return all_users_statistics
        

def statistics_for_all_users(course_id):
    course_users = usernames_manager.get_usernames(course_id)

    all_users_statistics = []
    users_with_placements = list()
    for user in course_users:
        user_stats = get_statistics(user["id"])
        user_team_tot_avg = mean([answer["tavg"] for answer in user_stats["numeric_feedback"]])
        user_global_tot_avg = mean([answer["gavg"] for answer in user_stats["numeric_feedback"]])
        users_with_placements.append((user_stats, user_team_tot_avg, user_global_tot_avg))      
        all_users_statistics.append(user_stats)
    users_with_placements.sort(key=lambda x: x[1])
    course_teams = teams_manager.get_teams(course_id)
    for team in course_teams:
        team_users = [(tuser[0], tuser[1]) for tuser in users_with_placements if tuser[0]["team_name"] == team["name"]]
        team_users.sort(key=lambda x: x[1])
        for i, user in enumerate(team_users):
            user[0]["team_placement"] = i+1
    users_with_placements.sort(key=lambda x: x[2])
    for i, user in enumerate(users_with_placements):
        user[0]["global_placement"] = i+1
    calculate_precentage(course_id, all_users_statistics, course_teams)
    return all_users_statistics