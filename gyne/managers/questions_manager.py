from ..models import Course, Form, Question


def get_questions(form_id: int):
    form_obj = Form.objects.get(id=form_id)
    form_questions = Question.objects.filter(form=form_obj)
    return [
        {
            "id": question.id,
            "desc": question.descr,
            "is_multipoint": question.is_multipoint,
            "text": question.question_text
        } for question in form_questions]


def get_text_questions(form_title, course_name):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Form.objects.get(course=course_obj, title=form_title)

    return [qst.question_text for qst in Question.objects.filter(form=form_obj) if qst.is_multipoint == False]


def get_multipoints_questions(form_title, course_name):
    course_obj = Course.objects.get(name=course_name)
    form_obj = Form.objects.get(course=course_obj, title=form_title)

    return [qst.question_text for qst in Question.objects.filter(form=form_obj) if qst.is_multipoint == True]
