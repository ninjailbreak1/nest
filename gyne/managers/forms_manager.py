from ..models import Course, Form, User, Team
from ..managers.questions_manager import get_questions
from ..managers.courses_manager import get_course


def get_form(user_id: int):
    user_obj = User.objects.get(id=user_id)
    active_form = user_obj.team.course.active_form

    return {
        'id': active_form.id,
        'title': active_form.title,
        'questions': get_questions(active_form.id),
        'course': get_course(user_obj.team.course.id),
        'max_rate': active_form.max_rate
    }

def get_form_list():
    forms = Form.objects.all()
    form_list = {}
    for form in forms:
        if form.course.name not in form_list:
            form_list[form.course.name] = []    
        form_list[form.course.name].append(form.name)
    return [{
        'name': course,
        'forms': forms
    } for course, forms in form_list.items()]


def get_form_max_rate(form_title, course_name):
    course_obj = Course.objects.get(name=course_name)
    return Form.objects.get(course=course_obj, title=form_title).max_rate
