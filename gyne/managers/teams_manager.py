from ..models import Course, Team
from .usernames_manager import get_usernames

def get_teams(course_id: int):
    course_obj = Course.objects.get(id=course_id)
    course_teams = Team.objects.filter(course=course_obj)

    return [{"name": team.name, "users": get_usernames(course_id, team.id)} for team in course_teams]