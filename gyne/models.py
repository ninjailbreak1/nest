#!/usr/bin/python
# -*- encoding: utf-8 -*-
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from enum import Enum

class QuestionType(Enum):
    '''
        There are one than one question type, therefore. numeric meaning is there a scale from X to Y
        Text field if there are addintional info to fill in
    '''
    numeric = 0b001
    text_field = 0b010


class Course(models.Model):
    """
    Every course has its name and the creation data
    Ex: "Kakas B", 17.5.2020
    """
    name = models.CharField(max_length=50, unique=True)
    creation_date = models.DateTimeField("Date created")
    active_form = models.OneToOneField("Form", on_delete=models.CASCADE, related_name="_active_form", null=True)
    def __str__(self):
        return self.name

class Form(models.Model):
    title = models.CharField(max_length=50)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    creation_date = models.DateTimeField("date created")
    max_rate = models.IntegerField(default=5)
    def __str__(self):
        return self.title


class Question(models.Model):
    form = models.ForeignKey(Form, on_delete=models.CASCADE)
    descr = models.TextField(max_length=1024)
    # Use L{QuestionType}
    is_multipoint = models.BooleanField(default=True)
    question_text = models.TextField(max_length=512)
    def __str__(self):
        return self.question_text


class Team(models.Model):
    name = models.CharField(max_length=32)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    def __str__(self):
        return self.name


class User(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    personal_number = models.CharField(max_length=1024, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Result(models.Model): 
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='_desc')
    victim = models.ForeignKey(User, on_delete=models.CASCADE)
    question_answered = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.TextField(max_length=1024, default="")
    def __str__(self):
        return f"{self.question_answered.question_text} בעבור {self.victim}"


