import json
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .managers import courses_manager, forms_manager, results_manager
from rest_framework.decorators import api_view
from rest_framework.response import Response


# Create your views here.

# REstful API for creating session
@api_view(['GET'])
def get_users(request):   
    course_id = request.GET.get('course_id')
    if not (course_id and course_id.isnumeric()):
        return Response([])
    
    # TODO: validate course_id in courses
    return Response(common.get_usernames(int(course_id)))


@api_view(['POST'])
def login(request):
    user_id = request.POST.get('user_id')
    # TODO: validate user_id is user
    request.session['user_id'] = int(user_id)
    return Response([])


@api_view(['GET'])
def get_courses(request):
    return Response(courses_manager.get_courses())


@api_view(['GET'])
def get_form(request):
    if not request.session['user_id']:
        return Response([])

    # TODO: validate user_id is user
    return Response(forms_manager.get_form(request.session['user_id']))

@api_view(['POST'])
def submit_form(reqeust):
    form = reqeust.POST.get('form')
    results_manager.parse_results(json.loads(form))
    return Response([])


@api_view(['GET'])
def get_stats(request):
    user_id = request.POST.get('user_id')
    # TODO: validate user_id is user
    results_manager.get_statistics(use)

@api_view(['GET'])
def get_form_list(request):
    return Response(forms_manager.get_form_list())


'''
def get_form_title(request):
    # TODO REGEV PUT HERE SOMETHING
    template = loader.get_template('index.html')
    name = request.GET.get('course_name')
    request.session['active_form'] = name

    return HttpResponse(template.render(


def get_form_question(request):
#     # TODO REGEV PUT HERE SOMETHING
#     template = loader.get_template('index.html')
#     name = request.session['course_name']
#     return HttpResponse(template.render(
#         {
#             'usernames': common.get_usernames(name),
#         }, request))


def get_question_desc(request):
    # TODO REGEV PUT HERE SOMETHING
    template = loader.get_template('index.html')
    form = request.session['active_form']
    name = request.session['course_name']
    return HttpResponse(template.render(
        {
            'question_desc': common.get_question_desc(form, name),
        }, request))


'''
