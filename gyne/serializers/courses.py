from rest_framework import serializers
from ..models import Course


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Course
        fields = ('name',)