from django.contrib import admin

# Register your models here.
from .models import Course, Form, Question, Team, User, Result

class GeneralAdmin(admin.ModelAdmin):
    pass

class ResultAdmin(GeneralAdmin):
    def get_victim(self, obj):
        return f"{obj.victim.first_name} {obj.victim.last_name}"
    get_victim.short_description = "Victim"
    get_victim.admin_order_field = "victim__first_name"
    
    def get_question(self, obj):
        return obj.question_answered.question_text
    get_question.short_description = "Question"
    get_question.admin_order_field = "question_answered__question_text"

    def get_answer(self, obj):
        return obj.answer
    get_answer.short_description = "Answer"
    get_answer.admin_order_field = "answer"
    
    list_filter = ("victim", "question_answered__question_text")
    list_display = ("get_victim", "get_question", "get_answer")



admin.site.register(Course)
admin.site.register(Form)
admin.site.register(Question)
admin.site.register(Team)
admin.site.register(User)
admin.site.register(Result, ResultAdmin)