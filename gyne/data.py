from gyne.managers import results_manager
a = {
    "user_id":1,
    "answers":
    [
        {
            "question_id": 1,
            "feedbacks": 
            [
                {
                    "victim_id": 7,
                    "answer": "3",
                },
                {
                    "victim_id": 8,
                    "answer": "4",
                },
                {
                    "victim_id": 9,
                    "answer": "7",
                },                   
            ]
        },
        {
            "question_id": 2,
            "feedbacks": 
            [
                {
                    "victim_id": 7,
                    "answer": "3",
                },
                {
                    "victim_id": 8,
                    "answer": "4",
                },
                {
                    "victim_id": 9,
                    "answer": "7",
                },                
            ]
        },
        {
            "question_id": 3,
            "feedbacks": 
            [
                {
                    "victim_id": 7,
                    "answer": "3",
                },
                {
                    "victim_id": 8,
                    "answer": "4",
                },
                {
                    "victim_id": 9,
                    "answer": "7",
                },               
            ]
        },
        {
            "question_id": 4,
            "feedbacks": 
            [
                {
                    "victim_id": 7,
                    "answer": "3",
                },
                {
                    "victim_id": 8,
                    "answer": "4",
                },
                {
                    "victim_id": 9,
                    "answer": "7",
                },                
            ]
        },
        {
            "question_id": 5,
            "feedbacks": 
            [
                {
                    "victim_id": 7,
                    "answer": "3",
                },
                {
                    "victim_id": 8,
                    "answer": "4",
                },
                {
                    "victim_id": 9,
                    "answer": "7",
                },              
            ]
        },
        {
            "question_id": 6,
            "feedbacks": 
            [
                {
                    "victim_id": 7,
                    "answer": "3",
                },
                {
                    "victim_id": 8,
                    "answer": "4",
                },
                {
                    "victim_id": 9,
                    "answer": "7",
                },                
            ]
        },
    ]
}

results_manager.parse_results(a)